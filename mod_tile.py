import yaml
import subprocess
import argparse
from copy import deepcopy


with open("mod_tile.yml", 'r') as f:
    mod_tile = yaml.load(f)
    f.close()

new_mod_tile = deepcopy(mod_tile)

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument(
    '-s', '--step', type=int, dest='step'
)

args = parser.parse_args()

if args.step == 1:
    container_wanted = ['sql', 'bright', 'carto']

if args.step == 2:
    container_wanted = ['sql', 'osm2pgsql', 'bright']

if args.step == 3:
    container_wanted = ['sql', 'apache2', 'renderd']

for services in mod_tile['services'].keys():
    if services not in container_wanted:
        del new_mod_tile['services'][services]

with open("docker-compose.yml", 'w') as f:
    yaml.dump(new_mod_tile, f, indent=10)
    f.close()

subprocess.call(['./mod_tile.sh'])
