#! /bin/bash
set -e

mkdir -p /var/run/apache2

export APACHE_RUN_DIR='/var/run/apache2'
export APACHE_PID_FILE='/var/run/apache2/apache2.pid'
export APACHE_RUN_USER='www-data'
export APACHE_RUN_GROUP='www-data'
export APACHE_LOG_DIR='/var/log/apache2'

# wait for renderd to be up
sleep 6

echo "Starting Apache2"
/usr/sbin/apache2 -e debug -DFOREGROUND
