#! /bin/bash
set -e

XML_PATH='/tmp/bright/build/mapnik.xml'
CARTO_PATH='/tmp/bright/build/project.mml'
CARTO_REPO_DEST='/tmp/osm-bright/'
CONGIF_PATH='/tmp/bright/configure.py'

if [[ ! -d "/tmp/bright/shp" ]]
then
    echo "The shp directory isn't found"
    if [[ ! -d "${CARTO_REPO_DEST}" ]]
    then
        echo "Cloning osm-bright"
        cd /tmp/
        git clone https://github.com/mapbox/osm-bright
        cp -r /tmp/osm-bright/* /tmp/bright
    fi
    
    echo "Making the shp directory"
    mkdir /tmp/bright/shp
    cd /tmp/bright/shp

    echo "Downloading the scripts"
    wget http://data.openstreetmapdata.com/simplified-land-polygons-complete-3857.zip
    wget http://data.openstreetmapdata.com/land-polygons-split-3857.zip

    echo "Extract the files and launch the scripts"
    unzip \*.zip
    shapeindex /tmp/bright/shp/simplified-land-polygons-complete-3857/simplified_land_polygons.shp
    shapeindex /tmp/bright/shp/land-polygons-split-3857/land_polygons.shp
    
    echo "Downloading another script needed if we use osm2pgsql"
    mkdir /tmp/bright/shp/10m-populated-places-simple
    cd /tmp/bright/shp/10m-populated-places-simple
    wget http://mapbox-geodata.s3.amazonaws.com/natural-earth-1.4.0/cultural/10m-populated-places-simple.zip
    
    echo "Extract the file and launch the script"
    unzip \*.zip
    shapeindex 10m-populated-places-simple.shp
fi

if [[ ! -e "${CONGIF_PATH}" ]]
then
    echo "${CONGIF_PATH} not found, making it"
    cp ${CONGIF_PATH}.sample ${CONGIF_PATH}
    
    echo "changing ${CONGIF_PATH} to match with our database's config"
    sed -i 's/config.*postgis.*host.*/config["postgis"]["host"]     = "sql"/' ${CONGIF_PATH}
    sed -i 's/config.*postgis.*dbname.*/config["postgis"]["dbname"]   = "gis"/' ${CONGIF_PATH}
    sed -i 's/config.*postgis.*user.*/config["postgis"]["user"]     = "gis"/' ${CONGIF_PATH}
    
    echo "deleting the prefix 'ne_' which isn't present in our file"
    sed -i 's/ne_10m_populated_places/10m-populated-places-simple/' ${CONGIF_PATH}
    sed -i 's/ne_10m_populated_places.shp/10m-populated-places-simple.shp/' ${CONGIF_PATH}
fi

if [[ ! -d "/tmp/bright/build/" ]]
then
    echo "/tmp/bright/build/ not found, generating it with make.py"
    cd /tmp/bright/
    ./make.py
fi

if [[ ! -e "/tmp/bright/build/mapnik.xml" ]]
then
    echo "${XML_PATH} not found, generating the XML FILE"
    carto ${CARTO_PATH} > ${XML_PATH}
    echo '*****************************************************'
    echo '*               DONE GENERATING XML FILE            *'
    echo '*****************************************************'
fi
