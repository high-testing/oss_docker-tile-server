#! /bin/bash
set -e

rm -f /var/run/renderd/renderd.sock
rm -f /var/run/renderd/renderd.stats

/usr/local/bin/renderd -f -c /usr/local/etc/renderd/renderd.conf
