#! /bin/bash
set -e

DATA_FILE_SOURCE='https://download.bbbike.org/osm/bbbike/Marseille/Marseille.osm.pbf'
DATA_FILE_DEST='/data/Marseille.osm.pbf'

if [[ ! -e "${DATA_FILE_DEST}" ]]
then
    echo "${DATA_FILE_DEST} not found, download and import into PG"
    wget -O ${DATA_FILE_DEST} ${DATA_FILE_SOURCE}
    
    echo '***************************************************'
    echo '* DONE DOWNLOADING PBF FILE - IMPORTING INTO PG   *'
    echo '***************************************************'
    
    # http://www.volkerschatz.com/net/osm/osm2pgsql-usage.html
    osm2pgsql -d gis -H sql -U gis --create --slim -G --hstore-all \
        -C 2500 --number-processes 2 \
        --tag-transform-script /tmp/carto/openstreetmap-carto.lua \
        --style /tmp/carto/openstreetmap-carto.style \
        ${DATA_FILE_DEST}

#    if you don't have openstreetmap-carto-css 
#    Using the default .lua and .style of osm2pgsql
#    osm2pgsql -d gis -H sql -U gis --create --slim -G --hstore-all \
#        -C 2500 --number-processes 2 \
#        --tag-transform-script /tmp/osm2pgsql/style.lua \
#        --style /tmp/osm2pgsql/default.style \
#        ${DATA_FILE_DEST}


else
    echo "${DATA_FILE_DEST} found, doing nothing."
fi
