#! /bin/bash
set -e

XML_PATH='/tmp/carto/mapnik.xml'
CARTO_PATH='/tmp/carto/project.mml'
CARTO_REPO='https://github.com/gravitystorm/openstreetmap-carto.git'
CARTO_REPO_DEST='/tmp/openstreetmap-carto/'

if [[ ! -d "${CARTO_REPO_DEST}" ]]
then
    git clone ${CARTO_REPO} ${CARTO_REPO_DEST}
fi
    
if [[ ! -e "${CARTO_PATH}" ]]
then
    cp -r ${CARTO_REPO_DEST}* /tmp/carto/
fi

if [[ ! -d "/tmp/carto/data/" ]]
then
    echo "Script not found"
    /tmp/carto/scripts/get-shapefiles.py -s
fi

if [[ ! -e "${XML_PATH}" ]]
then
    echo "No XML file found, generating to ${XML_PATH}"
    sed -i 's/dbname: "gis"/dbname: "gis"\n    host: "sql"\n    user: "gis"/' ${CARTO_PATH}
    carto ${CARTO_PATH} > ${XML_PATH}
    
    echo "Adding some tags in the STYLE file"
    sed -i 's/way        z_order/node,way   z_order/' /tmp/carto/openstreetmap-carto.style
    echo "node,way   population   text         linear" >> /tmp/carto/openstreetmap-carto.style

    echo '*****************************************************'
    echo '*               DONE GENERATING XML FILE            *'
    echo '*****************************************************'
else
    echo "XML file found at ${XML_PATH}. Doing nothing."
fi
