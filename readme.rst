###############
Map Tile Server
###############

A map tile server setup with all moving bits included.

=======
Running
=======

Using a virtual environment is highly recommended

To install the module needed for our python script :

::
    pip3 install -r requirement.txt

Run mod_tile.py

::
  ./mod_tile.sh --step <int=step_number>
  
Step N°1:
    - Configuration of carto-css and bright and Initialisation of our database
        This process involves some sizable donwload and may take some time.
        bright should exited with a code 1, don't worry it's normal
        
        When carto has done generating the XML file and bright has exited, pass to next step
        
Step N°2:
    - Download and Import the data into our DataBase and Create the XML file of carto
        Pass to next step when Indicate to restart the Docker-compose
    
Step N°3 ( and last )
    - Start apache2 and renderd
        Enjoy your maps !
