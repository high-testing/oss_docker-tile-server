#! /bin/bash

# apache2 is weird ...
if [[ `docker ps -a | grep mod_tile_apache2` ]]; then
    docker rm -f mod_tile_apache2
fi

docker-compose -f docker-compose.yml up
